#!/usr/bin/python
import argparse
import re
CONFIG = re.compile(r'/\*\sconfiguration:\s*([a-z0-9A-Z_]+)\s*,\s*([a-z0-9A-Z_]+)\s*\*/')


def main():
    parser = argparse.ArgumentParser(description='Protest configurator.')
    parser.add_argument('in-header', help="the file name of the header to modify.")
    parser.add_argument('out-header', help="the file name of the resulting header.")
    parser.add_argument('--suite', help="configure the name for the 'suite' macro.")
    parser.add_argument('--ignored', help="configure the name for the 'ignored' macro.")
    parser.add_argument('--broken', help="configure the name for the 'broken' macro.")
    parser.add_argument('--test', help="configure the name for the 'test' macro.")
    parser.add_argument('--fail', help="configure the name for the 'fail' macro.")
    parser.add_argument('--raises', help="configure the name for the 'raises' macro.")
    parser.add_argument('--check', help="configure the name for the 'check' macro.")
    parser.add_argument('--watch', help="configure the name for the 'watch' macro.")
    args = parser.parse_args()

    lines = []
    infile = open(getattr(args, 'in-header'), 'r')
    for line in infile.readlines():
        m = CONFIG.search(line)
        if m:
            macro = m.group(1)
            old_name = m.group(2)
            new_name = getattr(args, macro)
            if new_name:
                line = line.replace(old_name, new_name).replace("configuration: " + new_name, "configuration: " + old_name)
        lines.append(line)

    infile.close()
    
    outfile = open(getattr(args, 'out-header'), 'w')
    outfile.writelines(lines)
    outfile.close()
    

if __name__ == '__main__':
    main()
