#include <csetjmp>
#include <csignal>
#include <vector>
#include <sstream>
#include <cctype>

// TODO: should find another solution for these #ifdef for include guards.
#if _MSC_VER
#  include <Windows.h>
#  include <Dbghelp.h>
#  define sigsetjmp(buf, val) setjmp(buf)
#  define siglongjmp longjmp
#  define sigjmp_buf jmp_buf
#  ifdef _COMPLEX_
#    define _PROTEST_COMPLEX_INCLUDED
#  endif
#  ifdef _BITSET_
#    define _PROTEST_BITSET_INCLUDED
#  endif
#  ifdef _DEQUE_
#    define _PROTEST_DEQUE_INCLUDED
#  endif
#  ifdef _VALARRAY_
#    define _PROTEST_VALARRAY_INCLUDED
#  endif
#  ifdef _LIST_
#    define _PROTEST_LIST_INCLUDED
#  endif
#  ifdef _STACK_
#    define _PROTEST_STACK_INCLUDED
#  endif
#  ifdef _SET_
#    define _PROTEST_SET_INCLUDED
#  endif
#  ifdef _QUEUE_
#    define _PROTEST_QUEUE_INCLUDED
#  endif
#  ifdef _MAP_
#    define _PROTEST_MAP_INCLUDED
#  endif
#else
#  ifdef _GLIBCXX_COMPLEX
#    define _PROTEST_COMPLEX_INCLUDED
#  endif
#  ifdef _GLIBCXX_BITSET
#    define _PROTEST_BITSET_INCLUDED
#  endif
#  ifdef _GLIBCXX_DEQUE
#    define _PROTEST_DEQUE_INCLUDED
#  endif
#  ifdef _GLIBCXX_VALARRAY
#    define _PROTEST_VALARRAY_INCLUDED
#  endif
#  ifdef _GLIBCXX_LIST
#    define _PROTEST_LIST_INCLUDED
#  endif
#  ifdef _GLIBCXX_STACK
#    define _PROTEST_STACK_INCLUDED
#  endif
#  ifdef _GLIBCXX_SET
#    define _PROTEST_SET_INCLUDED
#  endif
#  ifdef _GLIBCXX_QUEUE
#    define _PROTEST_QUEUE_INCLUDED
#  endif
#  ifdef _GLIBCXX_MAP
#    define _PROTEST_MAP_INCLUDED
#  endif
#endif 

#define _PROTEST_TOKENPASTE_IMPL(x, y) x ## y
#define _PROTEST_TOKENPASTE(x, y) _PROTEST_TOKENPASTE_IMPL(x, y)
#define _PROTEST_UNIQ(UNIQUE) _PROTEST_TOKENPASTE(_protest_ ## UNIQUE, __LINE__)
#define _PROTEST_TOSTRING_IMPL(x) #x
#define _PROTEST_TOSTRING(x) _PROTEST_TOSTRING_IMPL(x)
#define _PROTEST_KEY(key, arg) "\0\4\2protest\1\3\0" "\0\0\0\0\0\0\0\0" key "\0" arg \
  "\0" __FILE__ "\0" _PROTEST_TOSTRING(__LINE__) "\0"

// Puts a magic string literal in the non-const initialized memory
// section.  The assignment to the zero-th elment is to make sure that
// the compiler does not optimize away the data.
#define _PROTEST_GLOBAL_INFO(tag, arg)                  \
  static char _protest_info[] = _PROTEST_KEY(tag, arg); \
  _protest_info[0] = char(long(_protest_info));

// Defines a suite of tests. First argument is the name of the suite (a
// string literal), the rest of the arguments are optional and are base
// classes to be used as fixtures.
#define suite(NAME, ...) /* configuration: suite, suite */              \
  namespace {                                                           \
  struct _PROTEST_UNIQ(suite) : ::protest::Empty, ##__VA_ARGS__ {       \
    inline void do_run(protest::TestCase const& tc);                    \
    static void run(protest::TestCase const& tc) {                      \
      _PROTEST_UNIQ(suite)().do_run(tc);                                \
    }                                                                   \
  };                                                                    \
  static char _PROTEST_UNIQ(info)[] = _PROTEST_KEY("suite", NAME);      \
  static ::protest::SuiteRegistrator _PROTEST_UNIQ(reg)(&_PROTEST_UNIQ(suite)::run, \
                                                        _PROTEST_UNIQ(info)); \
  }                                                                     \
  void _PROTEST_UNIQ(suite)::do_run(protest::TestCase const& _warning_protest_suite_does_not_contain_any_testcases)

// Declares that a 'test' is known to be bad. A known bad test is run,
// but is reported as failed if it passes and reported as passed if it
// failes. Must be places just before the test that is broken.
#define broken(DESCR) /* configuration: broken, broken */    \
  { _PROTEST_GLOBAL_INFO("broken", DESCR) }

// Declares that a 'test' is ignored. Must be places just before the
// test that is ignored.
#define ignored(DESCR) /* configuration: ignored, ignored */    \
  { _PROTEST_GLOBAL_INFO("ignored", DESCR) }

// Declares a test. Must be inside the scope of a 'suite'. NAME must
// be a string literal. Here, the _warning_protest_... variable is used
// which makes the compiler emit a warning if a suite does not contain any
// tests. This is a dual use of this variable; it is primarily used for
// passing around test-specific information.
#define test(NAME) /* configuration: test, test */                      \
  if (::protest::context().should_run(NAME, __FILE__,  __LINE__, _warning_protest_suite_does_not_contain_any_testcases)) \
    for (volatile int _protest_state = 0; _protest_state < 2; _protest_state++) \
      if (_protest_state == 1) {                                        \
        _PROTEST_GLOBAL_INFO("test", NAME)                              \
          ::protest::context().test_state(::protest::Reporter::TEARDOWN_STARTED); \
      } else                                                            \
        if (setjmp(::protest::check_failed_jump_buffer()) != 0) {       \
          ::protest::context().test_state(::protest::Reporter::TEST_FAILED); \
        } else

// TODO: simply doing 'longjmp' here means that destructors won't be
// executed, which means that there are potential memory leaks when an
// assert fails. How to solve this?
#define fail() /* configuration: fail, fail */                          \
  do {                                                                  \
    ::protest::context().reporter->progress(__FILE__, __LINE__);        \
    longjmp(::protest::check_failed_jump_buffer(), 1);                  \
  } while(0)


#define PROTEST_MATCHER_BEGIN ::protest::Empty());
#define PROTEST_MATCHER_END (::protest::Empty()
#define PROTEST_MATCHER_FAIL(msg) do { _protest.set_details(msg); _protest_ok = false; } while (0)
#define PROTEST_MATCHER_PASS() do { _protest_ok = true; } while (0)

// A matcher for checking that an exception is thrown by the provided
// expression.
#define raises(expr, exception) /* configuration: raises, raises */     \
  PROTEST_MATCHER_BEGIN try { (void)(expr);                             \
    PROTEST_MATCHER_FAIL("expected to throw " #exception ", but didn't"); \
  } catch (exception) { PROTEST_MATCHER_PASS(); }                       \
  PROTEST_MATCHER_END

// This pragma is for supressing a warning emitted from the usage of
// 'expression' and < and << below. Unfortunately, Embedding _Pragma
// in the macro does not work in all circumstances. Bug in gcc?
#ifdef __GNUC__
#pragma GCC diagnostic ignored "-Wparentheses"
#endif

// Checks that an expression is true. The expression in separated into
// left- and right-hand side and each side is stringified.
#define check(expression) /* configuration: check, check */             \
  for (::protest::SeqVarContext _protest_iter(::protest::CheckSeqVar);  \
       !_protest_iter.done(); _protest_iter.inc())                      \
    for (::protest::CheckContext _protest(#expression); _protest.state < 2; _protest.state++) \
      if (_protest.state == 0) {                                        \
        ::protest::context().reporter->progress(__FILE__, __LINE__);    \
        bool _protest_ok = (_protest.lhs < expression << _protest.rhs); \
        if (_protest_ok) _protest.state = 2;                            \
        else goto _PROTEST_UNIQ(fail_label);                            \
        ::protest::context().reporter->progress(__FILE__, __LINE__ + 1); \
      } else if (_protest.state == 1) {                                 \
        ::protest::context().reporter->                                 \
          check_fail(#expression, _protest.details(),                   \
                     ::protest::stringify_seqvars(),                    \
                     _protest.message_stream.str());                    \
        _protest_iter.failed();                                         \
      } else _PROTEST_UNIQ(fail_label): _protest.message_stream << ""

// Report an expression. Note: the value of watch(foo) is foo.
#define watch(expression) /* configuration: watch, watch */             \
  ::protest::watch_expression(expression, #expression, __FILE__, __LINE__)



#ifndef PROTEST_HARD_DISABLE_DETECT_MEMLEAK
inline void* operator new(size_t size) throw (std::bad_alloc);
inline void* operator new[](size_t size) throw (std::bad_alloc);
inline void operator delete(void *p) throw();
inline void operator delete[](void *p) throw();
#endif


namespace protest {
namespace tostring {

struct Unstringifiable {
  template<typename T> Unstringifiable(T const&) { }
};

inline std::ostream& operator<<(std::ostream& os, Unstringifiable const&) {
  return os << "{?}";
}

template<typename T>
void iterable_stringify(const char* prefix, std::ostream& os, T const& begin, T const& end);

template<typename T>
void stringify(std::ostream& os, std::vector<T> const& value);

#ifdef _PROTEST_QUEUE_INCLUDED
template<typename T>
void stringify(std::ostream& os, std::priority_queue<T> const& value);
#endif

#ifdef _PROTEST_VALARRAY_INCLUDED
template<typename T>
void stringify(std::ostream& os, std::valarray<T> const& value);
#endif

#ifdef _PROTEST_COMPLEX_INCLUDED
template<typename T>
void stringify(std::ostream& os, std::complex<T> const& value);
#endif

#ifdef _PROTEST_MAP_INCLUDED
template<typename K, typename V>
void stringify(std::ostream& os, std::map<K, V> const& value);

template<typename K, typename V>
void stringify(std::ostream& os, std::multimap<K, V> const& value);
#endif

void stringify(std::ostream& os, std::string const& value);
void stringify(std::ostream& os, const char* const& value);
void stringify(std::ostream& os, bool const& value);
void stringify(std::ostream& os, unsigned char const& value);
void stringify(std::ostream& os, signed char const& value);

template<typename T>
void stringify(std::ostream& os, std::vector<T> const& value) {
  iterable_stringify("vector", os, value.begin(), value.end());
}

#ifdef _PROTEST_DEQUE_INCLUDED
template<typename T>
void stringify(std::ostream& os, std::deque<T> const& value) {
  iterable_stringify("deque", os, value.begin(), value.end());
}
#endif

#ifdef _PROTEST_BITSET_INCLUDED
template<size_t N>
void stringify(std::ostream& os, std::bitset<N> const& value) {
  os << "bitset(" << value << ")";
}
#endif

#ifdef _PROTEST_LIST_INCLUDED
template<typename T>
void stringify(std::ostream& os, std::list<T> const& value) {
  iterable_stringify("list", os, value.begin(), value.end());
}
#endif

#ifdef _PROTEST_STACK_INCLUDED
template<typename T>
void stringify(std::ostream& os, std::stack<T> const& value) {
  std::stack<T> stack = value;
  std::vector<T> copy;
  while (!stack.empty()) {
    copy.push_back(stack.top());
    stack.pop();
  }
  iterable_stringify("stack", os, copy.begin(), copy.end());
}
#endif

#ifdef _PROTEST_SET_INCLUDED
template<typename T>
void stringify(std::ostream& os, std::set<T> const& value) {
  iterable_stringify("set", os, value.begin(), value.end());
}

template<typename T>
void stringify(std::ostream& os, std::multiset<T> const& value) {
  iterable_stringify("multiset", os, value.begin(), value.end());
}
#endif

template<typename T>
void stringify(std::ostream& os, T const& value) {
  os << value;
}

#ifdef _PROTEST_COMPLEX_INCLUDED
template<typename T>
void stringify(std::ostream& os, std::complex<T> const& value) {
  os << "complex(";
  stringify(os, value.real());
  os << ", ";
  stringify(os, value.imag());
  os << ")";
}
#endif

template<typename T>
void iterable_stringify(const char* prefix, std::ostream& os, T const& begin, T const& end) {
  os << prefix << "(";
  for (T i = begin; i != end;) {
    stringify(os, *i);
    if (++i != end)
      os << ", ";
  }
  os << ")";
}

template<typename T>
void map_stringify(const char* prefix, std::ostream& os, T const& begin, T const& end) {
  os << prefix;
  for (T i = begin; i != end;) {
    stringify(os, i->first);
    os << ":";
    stringify(os, i->second);
    if (++i != end)
      os << ", ";
  }
  os << ")";
}

#ifdef _PROTEST_MAP_INCLUDED
template<typename K, typename V>
void stringify(std::ostream& os, std::map<K, V> const& value) {
  map_stringify("map(", os, value.begin(), value.end());
}

template<typename K, typename V>
void stringify(std::ostream& os, std::multimap<K, V> const& value) {
  map_stringify("multimap(", os, value.begin(), value.end());
}
#endif

#ifdef _PROTEST_QUEUE_INCLUDED
template<typename T>
void stringify(std::ostream& os, std::priority_queue<T> const& value) {
  std::priority_queue<T> copy = value;
  os << "priority_queue" << "(";
  while(!copy.empty()) {
    stringify(os, copy.top());
    copy.pop();
    if (!copy.empty())
      os << ", ";
  }
  os << ")";
}
#endif

#ifdef _PROTEST_VALARRAY_INCLUDED
template<typename T>
void stringify(std::ostream& os, std::valarray<T> const& value) {
  os << "valarray(";
  for (size_t i = 0; i < value.size();) {
    stringify(os, value[i]);
    if (++i < value.size())
      os << ", ";
  }
  os << ")";
}
#endif

template<typename T>
std::string tostring(bool hexify, T const& value) {
  std::stringstream ss;
  if (hexify)
    ss << std::showbase << std::hex;
  stringify(ss, value);
  return ss.str();
}
}


// vector, set, map, priorty_queue: all in one!
template<typename T>
class OrdSet {
  std::vector<T> values;
public:
  void add(T const& value);
  T const& operator[](int index) const;
  T const& lookup(T const& value);
  bool contains(T const& value);
  unsigned size() const;
  void clear();
};

class SeqVar;

// Used by the 'check' and 'test' macros to handle faililng checks.
jmp_buf& check_failed_jump_buffer();
// Used by the signal handler to abort a test-case that segfaults, etc.
sigjmp_buf& sigsegv_buffer();
void sigsegv_handler(int);


class MemoryLeakDetector {
  static const size_t SIZE = 1024 * 1024;
  struct Info { size_t size; void* ptr; };
  Info allocs[SIZE]; // 1M of concurrent allocs;
public:
  MemoryLeakDetector();
  inline void test_finished();
  inline void* alloc(size_t size) throw (std::bad_alloc);
  inline void free(void *p) throw();
};
MemoryLeakDetector& memoryleak();

// Classes and operators below are used by the 'check' macro to split
// the expression into left-hand side and right-hand side.
struct Empty { };
struct ExprGrabber : public std::stringstream {
  bool hexify;
  template<typename T>
  const T& operator<(T const& obj) {
    static_cast<std::stringstream&>(*this) << tostring::tostring(hexify, obj);
    return obj;
  }
  bool operator<(Empty const&) { return true; }
};
template<typename T>
T const& operator<<(T const& obj, ExprGrabber& grabber) {
  grabber < obj;
  return obj;
}
inline bool operator<<(Empty const&, ExprGrabber&) { return true; }


// The state needed by the 'check' macro.
struct CheckContext {
  int state;
  ExprGrabber lhs, rhs;
  std::stringstream message_stream;
  std::string override_details;
  std::string op;
  CheckContext(const char* expr_to_check);

  // Matchers can override the default details. This is done, e.g., by the
  // 'raises' matcher.
  void set_details(std::string const& d);

  // Generate a detailed description of the checked expression. The
  // operator used to compare lhs and rhs needs to be extracted/parsed
  // to do this.
  std::string details();
};

// Interface of reporters.
struct Reporter {
  virtual ~Reporter() { }
  enum TestState { SETUP_STARTED, // The setup of of the test is started.
                   TEST_BODY_STARTED, // The body of the test is started.
                   TEARDOWN_STARTED, // The teardown of the test is started.
                   // Test result. No more reporting done after this is reported.
                   TEST_PASSED, TEST_FAILED, TEST_CRASHED
  };
  virtual void test_ignored(std::string const& /*suite*/, std::string const& /*test*/,
                            std::string const& /*message*/) { }
  virtual void test_started(std::string const& /*suite*/, std::string const& /*test*/) { }
  virtual void progress(const char* /*file*/, size_t /*line*/) { }
  virtual void test_state(TestState /*state*/) { }
  virtual void check_fail(const char* /*expr*/, std::string const& /*details*/,
                          OrdSet<std::string> const& /*seqvars*/,
                          std::string const& /*message*/) { }
  virtual void watched_expression(std::string const& /*expr*/,
                                  std::string const& /*value*/) { }
  virtual void memory_leaked(size_t /*bytes*/) { }
};


// Represents a single test-case implemented using the 'test' macro.
struct TestCase {
  std::string name, file;
  size_t line;
  const char* ignored_message; // == NULL => not ignored.
  const char* broken_message;  // == NULL => not broken.
  void (*runner)(TestCase const&); // The run method of the class that 'suite` expands to.
  TestCase(std::string const& name, std::string const& file, size_t line,
           const char* ignored_message, const char* broken_message,
	   void (*runner)(TestCase const&));
};


enum SeqVarScope { CheckSeqVar, TestSeqVar };

// The main context of Protest.
class Context {
  Reporter::TestState final_test_state;

  // Are we currently executing code in a test? This is false if the
  // testcase calls into the framework.
  bool in_test;

  OrdSet<SeqVar*> used_check_seqvars;
  OrdSet<SeqVar*> used_test_seqvars;
public:
  // Public fields for configuring Protest.
  Reporter* reporter;
  bool detect_memleaks;
  bool handle_signals;
  bool handle_exceptions;
  Context();
  bool test_active() const;

  // Used by the 'test' macro to determine which test to run.
  bool should_run(const char* test_name, const char* file_name, size_t line, TestCase const& tc);
  bool run_test(std::string const& suite, std::string const& test, TestCase const& testcase);

  // Used by tests to report their current state.
  void test_state(Reporter::TestState state);

  // Sequence variables
  OrdSet<SeqVar*>& seqvars(SeqVarScope scope);
};

Context& context();

class IntSeq {

};

// SeqVars are used for comparing arrays, among other things.
class SeqVar {
  virtual bool done() const = 0;
  virtual void reset() = 0;
  virtual void inc() = 0;
  const SeqVarScope scope;
  friend struct SeqVarContext;
protected:
  bool should_configure() {
    // Only reconfigure Check sequence variables or Test sequence
    // varible that haven't been configured yet.
    return ((scope == CheckSeqVar) || (scope == TestSeqVar && !context().seqvars(scope).contains(this)));
  }
  void register_used() { context().seqvars(scope).add(this); }
public:
  virtual void stringify(std::ostream& os) const = 0;
  const char* const name;
  SeqVar(const char* name, SeqVarScope scope) : scope(scope), name(name) { }
};

class IntSeqVar : public SeqVar {
  int begin, end, step, it;
  IntSeqVar(SeqVar const&);
  void configure(int b, int e, int s)  {
    if (!should_configure())
      return;
    it = begin = b;
    end = e;
    step = s;
  }
public:
  IntSeqVar(const char* name, SeqVarScope scope) : SeqVar(name, scope) { }
  bool done() const { return !(it < end); }
  void reset() { it = begin; }
  void inc() { it += step; }
  void operator()(int e) { configure(0, e, 1); }
  void operator()(int b, int e, int s = 1) { configure(b, e, s); }
  operator int() { register_used(); return it; }
  void stringify(std::ostream& os) const { os << it; }
};
static IntSeqVar I("I", CheckSeqVar), J("J", CheckSeqVar), K("K", CheckSeqVar);
static IntSeqVar X("X", TestSeqVar), Y("Y", TestSeqVar), Z("Z", TestSeqVar);

// Stringfy all used sequence variables and put them in a map of
// name-value pairs.
OrdSet<std::string> stringify_seqvars();

// Instantiated once for each 'check' and ech 'test' macro. Keeps track of all
// SeqVars that is used by the 'check' and the 'test' macro.
struct SeqVarContext {
  bool first, check_failed;
  SeqVarScope scope;
  SeqVarContext(SeqVarScope scope);
  bool done();
  void inc();
  void failed();
};

// Used by the 'watch' macro.
template<typename T>
T const& watch_expression(T const& value, const char* exprstr, const char* file, size_t line) {
  context().reporter->progress(file, line);
  std::stringstream ss;
  ss << value;
  context().reporter->watched_expression(exprstr, ss.str());
  return value;
}


// Used by the 'suite' macro for automatically registering test
// suites. This is done by writing the pointer to the runner function
// to 'global_data', which is a pointer to the initialized data
// section of the program. Furthermore, this data is tagged with a
// magic sequence of bytes (not done here) that is later used to
// search for registered tests in populate_suites.
struct SuiteRegistrator {
  SuiteRegistrator(void (*runner)(TestCase const&), char* global_data);
};


}

// Member function implementations
#if defined(PROTEST_USER_DEFINED_MAIN) | defined(PROTEST_MAIN)
#include <cstdlib>
#include <cstring>

// initialized_data_section_range needs to be ported for each platform.
extern "C" {
inline void initialized_data_section_range(const char** start, const char** end) {
#ifdef __CYGWIN32__
  extern const char _data_start__, _data_end__;
  *start = &_data_start__;
  *end = &_data_end__;
#elif defined(__linux)
  extern const char data_start, edata;
  *start = &data_start;
  *end = &edata;
#elif _MSC_VER
  // Adapted from http://stackoverflow.com/questions/4308996/finding-the-address-range-of-the-data-segment
  extern const char __ImageBase;
  // Get the address of NT Header
  IMAGE_NT_HEADERS* pNtHdr = ImageNtHeader((HINSTANCE)&__ImageBase);

  // After NT headers comes the table of section, so get the addess of section table
  IMAGE_SECTION_HEADER* pSectionHdr = (IMAGE_SECTION_HEADER*) (pNtHdr + 1);

  // Iterate through the list of all sections, and check the section name in the if conditon. etc
  for (int i = 0; i < pNtHdr->FileHeader.NumberOfSections; i++) {
    if (memcmp(pSectionHdr->Name, ".data", 5) == 0) {
      *start = &__ImageBase + pSectionHdr->VirtualAddress;
      *end = *start + pSectionHdr->Misc.VirtualSize;
      return;
    }
    pSectionHdr++;
  }
#else
#  error Unsupported OS and/or compiler
#endif
}
}


namespace protest {

namespace tostring {
void stringify(std::ostream& os, std::string const& value) { os << '"' << value << '"'; }
void stringify(std::ostream& os, const char* const& value) { stringify(os, std::string(value)); }
void stringify(std::ostream& os, bool const& value) { os << (value ? "true" : "false"); }
void stringify(std::ostream& os, unsigned char const& value) {
  os << unsigned(value);
  if (isprint(value))
    os << " (" << value << ")" ;
}
void stringify(std::ostream& os, signed char const& value) {
  os << signed(value);
  if (isprint(value))
    os << " (" << value << ")" ;
}
}

TestCase::TestCase(std::string const& name, std::string const& file, size_t line,
		   const char* ignored_message, const char* broken_message,
		   void (*runner)(TestCase const&)) :
  name(name), file(file), line(line), ignored_message(ignored_message),
  broken_message(broken_message), runner(runner) {
}


jmp_buf& check_failed_jump_buffer() {
  static jmp_buf b;
  return b;
}

sigjmp_buf& sigsegv_buffer() {
  static sigjmp_buf b;
  return b;
}

void sigsegv_handler(int) {
  siglongjmp(sigsegv_buffer(), 1);
}

template<typename T>
void OrdSet<T>::add(T const& value) {
  int index = 0;
  int size = values.size();
  for (; index < size; index++) {
    if (value < values[index])
      break;
    else if (values[index] == value)
      return;
  }
  values.insert(values.begin() + index, value);
}

template<typename T>
T const& OrdSet<T>::operator[](int index) const {
  return values[index];
}

template<typename T>
T const& OrdSet<T>::lookup(T const& value) {
  unsigned size = values.size();
  for (unsigned index = 0; index < size; index++) {
    if (values[index] == value)
      return values[index];
  }
  return value;
}

template<typename T>
bool OrdSet<T>::contains(T const& value) {
  unsigned size = values.size();
  for (unsigned index = 0; index < size; index++) {
    if (values[index] == value)
      return true;
  }
  return false;
}

template<typename T>
unsigned OrdSet<T>::size() const { return values.size(); }
template<typename T>
void OrdSet<T>::clear() { values.clear(); }


SuiteRegistrator::SuiteRegistrator(void (*runner)(TestCase const&), char* global_data) {
  memcpy(&global_data[13], (char*)&runner, sizeof(runner));
}


void do_stringify_seqvars(OrdSet<std::string>& map, OrdSet<SeqVar*>& seqvars) {
    for (unsigned i = 0; i < seqvars.size(); i++) {
    std::stringstream ss;
    ss << seqvars[i]->name << " == ";
    seqvars[i]->stringify(ss);
    map.add(ss.str());    
  }
}

OrdSet<std::string> stringify_seqvars() {
  OrdSet<std::string> map;
  do_stringify_seqvars(map, context().seqvars(CheckSeqVar));
  do_stringify_seqvars(map, context().seqvars(TestSeqVar));
  return map;
}

Context& context() {
  static Context context;
  return context;
}

SeqVarContext::SeqVarContext(SeqVarScope scope) : first(true), check_failed(false), scope(scope) {
  context().seqvars(scope).clear();
}

bool SeqVarContext::done() {
  if (first)
    return false;
  bool done = true;
  OrdSet<SeqVar*> seqs = context().seqvars(scope);
  for (unsigned i = 0; i < seqs.size(); i++) {
    done &= seqs[i]->done();
    if (seqs[i]->done())
      seqs[i]->reset();
  }
  if (done && check_failed)
    fail(); /* configuration: fail, fail */
  return done;
}

void SeqVarContext::inc() {
  first = false;
  OrdSet<SeqVar*> seqs = context().seqvars(scope);
  for (unsigned i = 0; i < seqs.size(); i++) {
    seqs[i]->inc();
    if (!seqs[i]->done())
      break;
  }
}
void SeqVarContext::failed() { check_failed = true; }

// Sets up and restores signal handlers.
struct HandleSignal {
  void (*original_handler_segv)(int);
  void (*original_handler_abrt)(int);
  HandleSignal(bool handle) : original_handler_segv(0), original_handler_abrt(0) {
    if (handle) {
      original_handler_segv = signal(SIGSEGV, &protest::sigsegv_handler);
      original_handler_abrt = signal(SIGABRT, &protest::sigsegv_handler);
    }
  }
  ~HandleSignal()  {
    if (original_handler_segv) {
      signal(SIGABRT, original_handler_abrt);
      signal(SIGSEGV, original_handler_segv);
    }
  }
};


Context::Context(): in_test(false), reporter(NULL), detect_memleaks(true),
                    handle_signals(true), handle_exceptions(true) { }

bool Context::test_active() const { return in_test; }

bool Context::should_run(const char* test_name, const char* file_name, size_t line, TestCase const& tc) {
  in_test = false;
  reporter->progress(file_name, line);
  bool run = (test_name == tc.name);
  if (run)
    reporter->test_state(Reporter::TEST_BODY_STARTED);
  in_test = true;
  return run;
}

void visit_testcases(void* context, void (*func)(void*, std::string const&, std::string const&, TestCase const&));

bool Context::run_test(std::string const& suitename, std::string const& testname, TestCase const& testcase) {
  if (testcase.ignored_message != NULL) {
    reporter->test_ignored(suitename, testname, testcase.ignored_message);
  } else {
    for (SeqVarContext ctx(TestSeqVar); !ctx.done(); ctx.inc()) {
      // Test is assumed to pass until test indicates otherwise.
      final_test_state = Reporter::TEST_PASSED;
      reporter->test_started(suitename, testname);
      reporter->test_state(Reporter::SETUP_STARTED);
      reporter->progress(testcase.file.c_str(), testcase.line);

      in_test = true;
      {
        HandleSignal handler(handle_signals);
        if (0 == sigsetjmp(protest::sigsegv_buffer(), 1)) {
          if (handle_exceptions) {
            try { testcase.runner(testcase); } catch (...) {
              protest::context().test_state(::protest::Reporter::TEST_CRASHED);
            }
          } else { testcase.runner(testcase); }
        } else {
          protest::context().test_state(::protest::Reporter::TEST_CRASHED);
        }
      }
      in_test = false;

      // Tell the memory leak detector that it should check for leaks and reset itself.
      protest::memoryleak().test_finished();

      // Tell the reporter the result of the test. No more reporting
      // should be done for this test.
      if (testcase.broken_message) {
	if (final_test_state == ::protest::Reporter::TEST_FAILED)
	  reporter->test_state(::protest::Reporter::TEST_PASSED);
	else if (final_test_state == ::protest::Reporter::TEST_PASSED)
	  reporter->test_state(::protest::Reporter::TEST_FAILED);
	else
	  abort();
      } else {
	reporter->test_state(final_test_state);
      }
    }
  }
  return true;
}

OrdSet<SeqVar*>& Context::seqvars(SeqVarScope scope) {
  if (scope == CheckSeqVar)
    return used_check_seqvars;
  return used_test_seqvars;
}

void Context::test_state(Reporter::TestState state) {
  if (state >= Reporter::TEST_FAILED) // Remember this and report it later.
    final_test_state = state;
  else
    reporter->test_state(state);
}

// Represents the information provided to the _PROTEST_GLOBAL_INFO macro.
struct GlobalInfo {
  void (*runner)(TestCase const&);
  const char* value;
  const char* tag_name;
  const char* file;
  size_t line;
  GlobalInfo(void (*runner)(TestCase const&), const char* value, const char* tag_name,
             const char* file, size_t line) :
    runner(runner), value(value), tag_name(tag_name), file(file), line(line) {
  }

  bool operator<(GlobalInfo const& rhs) const {
    int cmp = strcmp(rhs.file, file);
    if (cmp == 0)
      return line < rhs.line;
    return cmp < 0;
  }
  bool operator==(GlobalInfo const& rhs) const {
    return strcmp(rhs.file, file) == 0 && line == rhs.line;
  }

};


inline void visit_testcases(void* context, void (*func)(void*, std::string const&, std::string const&, TestCase const&)) {
  OrdSet<GlobalInfo> infos2;

  // Search for all 'protest' tags.
  const char *start, *end;
  initialized_data_section_range(&start, &end);
  for (const char* p = start; p != end; p++) {
    const char* start = strstr(p, "\4\2protest\1\3");
    if (start != NULL) {
      void (*runner)(TestCase const&);
      memcpy(&runner, &start[12], sizeof(runner));
      const char* tag = &start[12 + 8];
      const char* value = &tag[strlen(tag) + 1];
      const char* file = &value[strlen(value) + 1];
      int line = atoi(&file[strlen(file) + 1]);
      if (line > 0)
        infos2.add(GlobalInfo(runner, value, tag, file, line));
    }
  }

  const char* ignored_message = NULL;
  const char* broken_message = NULL;
  GlobalInfo current_suite(0, 0, 0, 0, 0);
  // Add each test to the correct suite and handle ignored test, etc.
  for (unsigned i = 0; i < infos2.size(); i++) {
    GlobalInfo const& info = infos2[i];
    if (strcmp(info.tag_name, "suite") == 0) {
      current_suite = info;
    } else if (strcmp(info.tag_name, "ignored") == 0) {
      ignored_message = info.value;
    } else if (strcmp(info.tag_name, "broken") == 0) {
      broken_message = info.value;
    } else if (strcmp(info.tag_name, "test") == 0) {
      TestCase tc(info.value, current_suite.file, current_suite.line,
		  ignored_message, broken_message, current_suite.runner);
      func(context, current_suite.value, info.value, tc);
      ignored_message = NULL;
      broken_message = NULL;
    }
  }
}

// Macro for searching through the allocation slots.
#define _PROTEST_FIND_ALLOC_SLOT(TEST_MUST_BE_ACTIVE, COND)             \
  if ((!TEST_MUST_BE_ACTIVE | protest::context().test_active()) &&      \
      protest::context().detect_memleaks)                               \
    for (size_t i = 0; i < SIZE; i++) if (allocs[i].ptr COND)


MemoryLeakDetector& memoryleak() {
  static MemoryLeakDetector detector;
  return detector;
}

MemoryLeakDetector::MemoryLeakDetector() {
  memset(allocs, 0, SIZE * sizeof(allocs[0]));
}

void MemoryLeakDetector::test_finished() {
  size_t leaked = 0;
  _PROTEST_FIND_ALLOC_SLOT(false, != NULL) { leaked += allocs[i].size; }
  memset(allocs, 0, SIZE * sizeof(allocs[0]));
  if (leaked > 0)
    protest::context().reporter->memory_leaked(leaked);
}

void* MemoryLeakDetector::alloc(size_t size) throw (std::bad_alloc) {
  void* p = ::malloc(size);
  if (p == 0)
    throw std::bad_alloc();
  _PROTEST_FIND_ALLOC_SLOT(true, == NULL) {
    allocs[i].size = size;
    allocs[i].ptr = p;
    break;
  }
  return p;
}

void MemoryLeakDetector::free(void *p) throw() {
  _PROTEST_FIND_ALLOC_SLOT(true, == p) {
    allocs[i].ptr = NULL;
    allocs[i].size = 0;
    break;
  }
  ::free(p);
}

static bool is_hex_literal(std::string const& s) {
  int pos = s.find_first_not_of(" ");
  if (pos >= 0 && (s[pos] != '0' || (s[pos + 1] != 'x' || s[pos + 1] != 'X')))
    return false;
  return std::string::npos == s.find_first_not_of(" 0xX0123456789abcdefABCDEF");
}

CheckContext::CheckContext(const char* expr_to_check) : state(0) {
  static const char* ops[] = { "==", "!=", ">=", "<=", ">", "<", NULL };
  int i = -1;
  bool hexify = false;
  const char* substr;
  while (ops[++i] != NULL && (substr = strstr(expr_to_check, ops[i])) == NULL);
  if (ops[i] != NULL) {
    op = ops[i];
    hexify = is_hex_literal(&substr[strlen(ops[i])]) || is_hex_literal(std::string(expr_to_check, substr - expr_to_check));
  }
  lhs.hexify = hexify;
  rhs.hexify = hexify;
}
void CheckContext::set_details(std::string const& d) { override_details = d; }
std::string CheckContext::details() {
  if (!override_details.empty())
    return override_details;
  if (op.empty())    
    return lhs.str();
  return lhs.str() + " " + op + " " + rhs.str();
}


#undef _PROTEST_FIND_ALLOC_SLOT
}

#ifndef PROTEST_HARD_DISABLE_DETECT_MEMLEAK
inline void* operator new(size_t size) throw (std::bad_alloc) {
  return protest::memoryleak().alloc(size);
}
inline void* operator new[](size_t size) throw (std::bad_alloc) {
  return protest::memoryleak().alloc(size);
}
inline void operator delete(void *p) throw() {
  return protest::memoryleak().free(p);
}
inline void operator delete[](void *p) throw() {
  return protest::memoryleak().free(p);
}
#endif


#include <iostream>
#include <fstream>

struct BriefReporter : public protest::Reporter {
  const char* file;
  size_t line;
  std::string suite;
  std::string test;

  ~BriefReporter() { std::cout << std::endl; }

  void test_ignored(std::string const&, std::string const&, std::string const&) {
    std::cout << "I" << std::flush;
  }

  void progress(const char* file, size_t line) {
    this->file = file;
    this->line = line;
  }

  void test_started(std::string const& suite, std::string const& test) {
    this->suite = suite;
    this->test = test;
  }

  void test_state(TestState state) {
    if (state == protest::Reporter::TEST_PASSED)
      std::cout << "." << std::flush;
    else if (state == protest::Reporter::TEST_FAILED)
      std::cout << "F" << std::flush;
    else if (state == protest::Reporter::TEST_CRASHED) {
      std::cout << std::endl << file << ":" << line << ": Crash at or after this point. ["
                << this->suite << "/" << this->test << "]" << std::endl;
      std::cout << "E" << std::flush;
    }
  }

  void check_fail(const char* expr, std::string const& details,
                  protest::OrdSet<std::string> const& seqvars,
                  std::string const& message) {
    std::cout << std::endl << file << ":" << line << ": " << expr <<
      " (" << details << ") failed";
    if (seqvars.size() != 0) {
      std::cout << " for ";
      for (unsigned i = 0; i < seqvars.size(); i++) {
        std::cout << seqvars[i];
        if (i != seqvars.size() - 1)
          std::cout << ", ";
      }
    }
    std::cout << ". [" << suite << "/" <<
      test << "][" << message << "]." << std::endl;
  }
};

struct XmlReporter : public protest::Reporter {
  const char* file;
  size_t line;
  std::string suite;
  std::string test;
  std::ofstream output;

  XmlReporter(const char* filename) : output(filename) {
    output << "<testsuite>" << std::endl;
  }
  ~XmlReporter() {
    output << "</testsuite>" << std::endl;
  }

  void progress(const char* file, size_t line) {
    this->file = file;
    this->line = line;
  }

  void test_started(std::string const& suite, std::string const& test) {
    output << "<testcase classname=\"" << suite << "\" name=\""
           << test << "\">" << std::endl;
  }

  void test_state(TestState state) {
    if (state >= protest::Reporter::TEST_PASSED)
      output << "</testcase>" << std::endl;
  }

  void check_fail(const char* expr, std::string const& details,
                  protest::OrdSet<std::string> const& /*seqvars*/,
                  std::string const& message) {
    output << "<failure type=\"assert\">" << "Assert fail: " << expr << " ("
           << details << "): " << message << "</failure>";
  }

  void memory_leaked(size_t bytes) {
    output << "<failure type=\"memory leak\">" << "Leaked " << bytes
           << " bytes." << "</failure>" << std::endl;
  }

};

#define PROTEST_INV_BOOL_ARG(name, short_name, inv_short_name, dest)    \
  if ((strcmp(argv[i], "--" name) == 0) || (strcmp(argv[i], "-" short_name) == 0)) \
    dest = true;                                                        \
  else if ((strcmp(argv[i], "--no-" name) == 0) || (strcmp(argv[i], "-" inv_short_name) == 0)) \
    dest = false;                                                       \
  else

#define PROTEST_BOOL_ARG(name, short_name, dest)                        \
  if ((strcmp(argv[i], "--" name) == 0) || (strcmp(argv[i], "-" short_name) == 0)) \
    dest = true;                                                        \
  else

#define PROTEST_VALUE_ARG(name, dest)           \
  if (strcmp(argv[i], "--" name) == 0) {        \
    if (i < argc - 1 && argv[i + 1][0] != '-')  \
      dest = argv[++i];                         \
    else                                        \
      printf(name " requires an argument.\n");  \
  } else

void print_test(void* counter, std::string const& suitename, std::string const& testname, protest::TestCase const&)  {
  (*reinterpret_cast<int*>(counter))++;
  printf("  %s/%s\n", suitename.c_str(), testname.c_str());
}

void run_test(void* _tests, std::string const& suitename, std::string const& testname, protest::TestCase const& tc)  {
  protest::OrdSet<std::string>& tests = *reinterpret_cast<protest::OrdSet<std::string>*>(_tests);
  if ((tests.size() == 0) || tests.contains(suitename + "/" + testname)) {
    protest::context().run_test(suitename, testname, tc);
  }
}
#endif

#ifdef PROTEST_MAIN
int main(int argc, const char** argv) {
  const char* xmlfile = NULL;
  protest::OrdSet<std::string> tests;
  bool show_help = false;
  bool list_tests = false;
  bool unkown_option = false;
  for (int i = 1; i < argc; i++) {
    PROTEST_BOOL_ARG("help", "h", show_help)
    PROTEST_BOOL_ARG("list", "l", list_tests)
    PROTEST_INV_BOOL_ARG("mem-check", "m", "M", protest::context().detect_memleaks)
    PROTEST_INV_BOOL_ARG("signal-check", "s", "S",  protest::context().handle_signals)
    PROTEST_INV_BOOL_ARG("exception-check", "e", "E", protest::context().handle_exceptions)
    PROTEST_VALUE_ARG("xml", xmlfile)
    if (argv[i][0] != '-') {
      tests.add(argv[i]);
    } else if (argv[i][0] == '-') {
      printf("Unknown argument: %s.\n", argv[i]);
      unkown_option = true;
    }
  }

  if (unkown_option)
    return 1;

  if (show_help) {
    printf("Usage %s [OPTION]... [TEST]...\n", argv[0]);
    printf("A Protest unit test runner.\n");
    printf("  -e, --exception-check     enable handling of exceptions (default)\n");
    printf("  -E, --no-exception-check  disable handling of exceptions\n");
    printf("  -l, --list                list all test-cases and exit\n");
    printf("  -m, --mem-check           enable memory leak detection (default)\n");
    printf("  -M, --no-mem-check        disable memory leak detection\n");
    printf("  -s, --signal-check        enable handling of signals (default)\n");
    printf("  -S, --no-signal-check     disable handling of signals\n");
    printf("      --xml FILE            report test result in JUnit xml format\n");
    printf("  -h  --help                print this help and exit\n\n");
    printf("Example:\n");
    printf("  %s 'test suite/first test'\n", argv[0]);
    printf("Protest homepage: <https://gitorious.org/protest>\n");
    return 0;
  }

  if (list_tests) {
    int num_tests = 0;
    visit_testcases(&num_tests, &print_test);
    printf("Total: %d test-cases\n", num_tests);
    return 0;
  }

  if (xmlfile)
    protest::context().reporter = new XmlReporter(xmlfile);
  else
    protest::context().reporter = new BriefReporter();

  visit_testcases(&tests, &run_test);

  delete protest::context().reporter;
}
#endif
