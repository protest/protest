#!/usr/bin/env bash

set -xe

ROOT_DIR=$(readlink -f $(dirname $0)/..)
cd ${ROOT_DIR}

g++ test/assert-messages.cc -o ./assert-messages
./config.py --suite SUITE --test TEST --check CHECK --fail FAIL --raises RAISES --watch WATCH --ignored IGNORED --broken BROKEN include/protest.hh include/protest-configured.hh
for CC in `ls /usr/bin/clang++*` `ls /usr/bin/g++*`
do
    for OPT in 0 3
    do
        $CC -O$OPT -Iinclude -g -Wall -Wextra test/broken_test.cc -o a.exe && ./a.exe
        $CC -O$OPT -Iinclude -g -Wall -Wextra test/suite_at_same_line_A_test.cc test/suite_at_same_line_B_test.cc -o a.exe && ./a.exe
        $CC -O$OPT -Iinclude -g -Wall -Wextra test/correct_jmp_buf_used/main_without_test_cases.cc test/correct_jmp_buf_used/single_test_case.cc -o a.exe && ./a.exe
        $CC -O$OPT -Iinclude -g -Wall -Wextra test/configuration_test.cc -o a.exe && ./a.exe
        $CC -O$OPT -Iinclude -g -Wall -Wextra test/matcher_test.cc -o a.exe && ./a.exe
        $CC -O$OPT -Iinclude -g -Wall -Wextra test/main_test.cc -o a.exe && ./a.exe
        $CC -O$OPT -Iinclude -g -Wall -Wextra test/stringify_test.cc -o a.exe && ./a.exe
        $CC -O$OPT -Iinclude -g -Wall -Wextra test/stringify_without_includes_test.cc -o a.exe && ./a.exe
        $CC -O$OPT -Iinclude -g -Wall -Wextra test/seqvars_check_test.cc -o a.exe && ./a.exe
        $CC -O$OPT -Iinclude -g -Wall -Wextra test/many_files_0.cc -c -o test/many_files_0.o
        $CC -O$OPT -Iinclude -g -Wall -Wextra test/many_files_1.cc -c -o test/many_files_1.o
        $CC -O$OPT -Iinclude -g -Wall -Wextra test/many_files_2.cc -c -o test/many_files_2.o
        $CC -O$OPT -Iinclude -g -Wall -Wextra test/many_files_test.cc -c -o test/many_files_test.o
        $CC test/many_files_0.o test/many_files_1.o test/many_files_2.o test/many_files_test.o -o a.exe && ./a.exe
        $CC -O$OPT -Iinclude -g -Wall -Wextra test/compiler_warnings_test.cc 2>&1 | ./assert-messages test/compiler_warnings_test.cc
        $CC -O$OPT -Iinclude -g -Wall -Wextra test/disable_sigsegv_handling.cc -o a.exe && ./a.exe
        $CC -O$OPT -Iinclude -g -Wall -Wextra test/detect_memory_leak_test.cc -o a.exe && ./a.exe
        $CC -O$OPT -Iinclude -g -Wall -Wextra test/thrown_exception_test.cc -o a.exe && ./a.exe
        $CC -O$OPT -Iinclude -g -Wall -Wextra test/report_progress_test.cc -o a.exe && ./a.exe
        $CC -O$OPT -Iinclude -g -Wall -Wextra test/handle_sigsegv_test.cc -o a.exe && (./a.exe 2>&1 | ./assert-messages test/handle_sigsegv_test.cc)
        $CC -O$OPT -Iinclude -g -Wall -Wextra test/check_macros_test.cc -o a.exe && ./a.exe
        $CC -O$OPT -Iinclude -g -Wall -Wextra test/split_expressions_test.cc -o a.exe && ./a.exe
        $CC -O$OPT -Iinclude -g -Wall -Wextra test/test_macro_test.cc -o a.exe && ./a.exe
        $CC -O$OPT -Iinclude -g -Wall -Wextra test/ignore_test.cc -o a.exe && ./a.exe
        $CC -O$OPT -Iinclude -g -Wall -Wextra test/test_progress_test.cc -o a.exe && ./a.exe
        $CC -O$OPT -Iinclude -g -Wall -Wextra test/watch_macro_test.cc -o a.exe && ./a.exe
    done
done
