#include <cassert>
#include <iostream>
#include <cstdio>
#include <string>
#include <vector>
#include <fstream>
#include <sstream>

int main(int argc, const char** argv) {
  if (argc != 2) {
    std::cout << "Expectes one argument." << std::endl;
    return -1;
  }

  const char* sourcename = argv[1];

  std::vector<std::string> lines;
  std::ifstream sourcefile(sourcename);
  std::string line;
  size_t linenum = 0;
  while (sourcefile) {
    linenum++;
    std::getline(sourcefile, line);
    if (line.find("//(expect message)") != std::string::npos) {
      std::stringstream ss;
      ss << linenum;
      lines.push_back(ss.str());
    }
  }


  while (std::cin) {
    std::getline(std::cin, line);
    unsigned i = 0;
    for (std::vector<std::string>::iterator it = lines.begin();
	 it != lines.end(); ++it) {
      if (line.find(*it) != std::string::npos) {
	lines.erase(it);
	break;
      }
      i++;
    }
  }

  for (size_t i = 0; i < lines.size(); i++) {
    std::cout << "*** Error: message on line " << lines[i] << " not found ***" << std::endl;
  }
  return lines.size();
}
