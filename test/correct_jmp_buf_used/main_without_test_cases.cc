/*
 * This test case caused segmentation fault as the global jump buffers
 * (jmp_buf and sigjmp_buf) were not deterministically singeltons.
 */
#define PROTEST_USER_DEFINED_MAIN

#include <protest.hh>
#include "../test-common.hh"
#include <cassert>

static bool test_failed = false;
struct Reporter : public ::protest::Reporter {
  void test_state(protest::Reporter::TestState state) {
    if (state == protest::Reporter::TEST_FAILED)
      ::test_failed = true; 
  }
};

int main() {
  Reporter reporter;
  protest::context().reporter = &reporter;
  run_test("foo", "bar");
  assert(test_failed);
}
