#define PROTEST_USER_DEFINED_MAIN

#include "protest.hh"
#include <cassert>
#include "test-common.hh"
#include <set>

struct Reporter : public protest::Reporter {
  std::set<std::string> details;
  std::vector<protest::OrdSet<std::string> > seqvars;
  int tests_started, tests_finished;

  Reporter() : tests_started(0), tests_finished(0) { }

  void check_fail(const char* /*expr*/, std::string const& details,
		  protest::OrdSet<std::string> const& seqvars,
		  std::string const& /*message*/) {
    this->details.insert(details);
    this->seqvars.push_back(seqvars);
  }
  
  void test_state(protest::Reporter::TestState state) {
    if (state == protest::Reporter::SETUP_STARTED)
      tests_started++;
    else if (state == protest::Reporter::TEARDOWN_STARTED)
      tests_finished++;
  }
  
};

using protest::I;
using protest::J;
using protest::K;

int times_record_called = 0;
bool record(int) {
  times_record_called++;
  return true;
}
suite("check seqvars") {
  test("test 0") {
    I(3);
    J(3);
    check(int(I) == int(I));
    check(record(J));
    check(record(int(J) + int(I)));
    check(int(I) == -1);
  }

  test("test 1") {
    J(2, 4);
    check(int(J) == -1);
  }

  test("test 2") {
    K(0, 3, 2);
    check(int(K) == -1);
  }

  test("test 3") {
    I(0, 2);
    J(0, 2);
    K(0, 2);
    check(I + J * 2 + K * 4 == -1);
  }
}

using protest::X;
using protest::Y;
using protest::Z;

std::multiset<int> integers;
suite("test seqvars") {
  test("test 0") {
    X(2);
    integers.insert(X);
  }

  test("test 1") {
    Y(1, 3);
    integers.insert(Y);
  }

  test("test 2") {
    Z(1, 4, 2);
    integers.insert(Z);
  }

  test("test 3") {
    X(2);
    Y(2);
    Z(2);
    integers.insert(X + Y * 2 + Z * 4);
  }

  test("test 4") {
    X(2);
    I(2);
    check(int(I) == int(X));
  }
}


// TODO: emit warning if sequence variables are used by testcases
// without configuration.
// TODO: emit warning if test sequence variables are configured
// more than once per test.
// TODO: emit warning if check sequence variables are used outside check.
int main() {
  Reporter reporter;
  protest::context().reporter = &reporter;
  run_test("check seqvars", "test 0");
  assert(times_record_called == 12);
  assert(reporter.details.size() == 3);
  assert(reporter.details.count("0 == -1"));
  assert(reporter.details.count("1 == -1"));
  assert(reporter.details.count("2 == -1"));
  assert(reporter.seqvars[0][0] == "I == 0");
  assert(reporter.seqvars[1][0] == "I == 1");
  assert(reporter.seqvars[2][0] == "I == 2");
  assert(reporter.tests_started == 1);
  assert(reporter.tests_finished == 1);

  reporter = Reporter();
  run_test("check seqvars", "test 1");
  assert(reporter.details.size() == 2);
  assert(reporter.details.count("2 == -1"));
  assert(reporter.details.count("3 == -1"));
  assert(reporter.tests_started == 1);
  assert(reporter.tests_finished == 1);


  reporter = Reporter();
  run_test("check seqvars", "test 2");
  assert(reporter.details.size() == 2);
  assert(reporter.details.count("0 == -1"));
  assert(reporter.details.count("2 == -1"));
  assert(reporter.tests_started == 1);
  assert(reporter.tests_finished == 1);

  reporter = Reporter();
  run_test("check seqvars", "test 3");
  assert(reporter.details.size() == 8);
  assert(reporter.details.count("0 == -1"));
  assert(reporter.details.count("1 == -1"));
  assert(reporter.details.count("2 == -1"));
  assert(reporter.details.count("3 == -1"));
  assert(reporter.details.count("4 == -1"));
  assert(reporter.details.count("5 == -1"));
  assert(reporter.details.count("6 == -1"));
  assert(reporter.details.count("7 == -1"));
  assert(reporter.seqvars[0][0] == "I == 0");
  assert(reporter.seqvars[0][1] == "J == 0");
  assert(reporter.seqvars[0][2] == "K == 0");
  assert(reporter.seqvars[7][0] == "I == 1");
  assert(reporter.seqvars[7][1] == "J == 1");
  assert(reporter.seqvars[7][2] == "K == 1");
  assert(reporter.tests_started == 1);
  assert(reporter.tests_finished == 1);

  integers.clear();
  reporter = Reporter();
  run_test("test seqvars", "test 0");
  assert(integers.size() == 2);
  assert(integers.count(0) == 1);
  assert(integers.count(1) == 1);
  assert(reporter.tests_started == 2);
  assert(reporter.tests_finished == 2);

  integers.clear();
  reporter = Reporter();
  run_test("test seqvars", "test 1");
  assert(integers.size() == 2);
  assert(integers.count(1) == 1);
  assert(integers.count(2) == 1);
  assert(reporter.tests_started == 2);
  assert(reporter.tests_finished == 2);

  integers.clear();
  reporter = Reporter();
  run_test("test seqvars", "test 2");
  assert(integers.size() == 2);
  assert(integers.count(1) == 1);
  assert(integers.count(3) == 1);
  assert(reporter.tests_started == 2);
  assert(reporter.tests_finished == 2);

  integers.clear();
  reporter = Reporter();
  run_test("test seqvars", "test 3");
  assert(integers.size() == 8);
  assert(integers.count(0));
  assert(integers.count(1));
  assert(integers.count(2));
  assert(integers.count(3));
  assert(integers.count(4));
  assert(integers.count(5));
  assert(integers.count(6));
  assert(integers.count(7));
  assert(reporter.tests_started == 8);
  assert(reporter.tests_finished == 8);

  reporter = Reporter();
  run_test("test seqvars", "test 4");
  assert(reporter.details.size() == 2);
  assert(reporter.details.count("0 == 1"));
  assert(reporter.details.count("1 == 0"));
  assert(reporter.seqvars[0][0] == "I == 1");
  assert(reporter.seqvars[0][1] == "X == 0");
  assert(reporter.seqvars[1][0] == "I == 0");
  assert(reporter.seqvars[1][1] == "X == 1");
  assert(reporter.tests_started == 2);
  assert(reporter.tests_finished == 2);

}
