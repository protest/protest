#define PROTEST_USER_DEFINED_MAIN
#include <stdio.h>
#include <protest.hh>
#include <string>
#include <cassert>
#include "test-common.hh"

struct Reporter : public ::protest::Reporter {
  std::string details;
  void check_fail(const char*,
                  std::string const& details,
                  protest::OrdSet<std::string> const& /*seqvars*/,
                  std::string const& /*message*/) {
      this->details = details;
  } 
};

suite("suite") {
  int i = 14;
  test("test 0") {
    check(i == 0xF);
  }
  test("test 1") {
    check(0xE != i);
  }
}


int main() {
  Reporter reporter;
  protest::context().reporter = &reporter;
  run_test("suite", "test 0");
  assert(reporter.details == "0xe == 0xf");

  run_test("suite", "test 1");
  assert(reporter.details == "0xe != 0xe");
}
