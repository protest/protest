#define PROTEST_USER_DEFINED_MAIN
#include "protest.hh"
#include <cassert>
#include "test-common.hh"

struct State {
  std::string suite_name;
  std::string test_name;
  protest::Reporter::TestState state;
  State(std::string const& suite_name, std::string const& test_name,
	protest::Reporter::TestState state) :
    suite_name(suite_name), test_name(test_name), state(state) {}
};

struct Reporter : public protest::Reporter {
  std::vector<State> states;
  std::string suite_name, test_name;
  
  void test_started(std::string const& suite_name, std::string const& test_name) {
    this->suite_name = suite_name;
    this->test_name = test_name;
  }

  void test_state(protest::Reporter::TestState state) {
    states.push_back(State(suite_name, test_name, state));
  }
};

Reporter reporter;
suite("suite 0") {
  State const& last = reporter.states[reporter.states.size() - 1];
  assert(last.suite_name == "suite 0");
  assert(last.state == protest::Reporter::SETUP_STARTED);

  test("test 0") {
    State const& last = reporter.states[reporter.states.size() - 1];
    assert(last.test_name == "test 0");
    assert(last.state == protest::Reporter::TEST_BODY_STARTED);
  }

  test("test 1") {
    State const& last = reporter.states[reporter.states.size() - 1];
    assert(last.test_name == "test 1");
    assert(last.state == protest::Reporter::TEST_BODY_STARTED);
    fail();
  }

  assert(reporter.states[reporter.states.size() - 1].state ==
	 protest::Reporter::TEARDOWN_STARTED);

}
int main() {
  protest::context().reporter = &reporter;
  run_test("suite 0", "test 0");

  assert(reporter.states[0].state == protest::Reporter::SETUP_STARTED);
  assert(reporter.states[1].state == protest::Reporter::TEST_BODY_STARTED);
  assert(reporter.states[2].state == protest::Reporter::TEARDOWN_STARTED);
  assert(reporter.states[3].state == protest::Reporter::TEST_PASSED);

  reporter = Reporter();
  run_test("suite 0", "test 1");

  assert(reporter.states[0].state == protest::Reporter::SETUP_STARTED);
  assert(reporter.states[1].state == protest::Reporter::TEST_BODY_STARTED);
  assert(reporter.states[2].state == protest::Reporter::TEARDOWN_STARTED);
  assert(reporter.states[3].state == protest::Reporter::TEST_FAILED);  
}
