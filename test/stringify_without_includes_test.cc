#include <cassert>
#include <iostream>

struct Foo { };
void stringify(std::ostream& os, Foo const&) {
  os << "Foo";
}

#define PROTEST_USER_DEFINED_MAIN
#include "protest-configured.hh"

#include <vector>
#include <list>
#include <set>
#include <map>
#include <bitset>
#include <complex>
#include <deque>
#include <queue>
#include <valarray>
#include <utility>
#include <stack>

int main() {
  assert("10" == protest::tostring::tostring(false, int(10)));
  assert("20" == protest::tostring::tostring(false, long(20)));
  assert("30" == protest::tostring::tostring(false, size_t(30)));
  assert("40" == protest::tostring::tostring(false, short(40)));
  assert("true" == protest::tostring::tostring(false, bool(1)));
  assert("false" == protest::tostring::tostring(false, bool(0)));
  
  assert("\"hello\"" == protest::tostring::tostring(false, "hello"));
  assert("\"hello\"" == protest::tostring::tostring(false, std::string("hello")));

  std::vector<std::string> ve;
  ve.push_back("one");
  ve.push_back("two");
  assert("vector(\"one\", \"two\")" == protest::tostring::tostring(false, ve));

  std::valarray<bool> va(2);
  va[0] = false;
  va[1] = true;
  assert("{?}" == protest::tostring::tostring(false, va));

  std::list<int> li;
  assert("{?}" == protest::tostring::tostring(false, li));

  std::stack<int> st;
  assert("{?}" == protest::tostring::tostring(false, st));

  std::vector<long> v0;
  v0.push_back(10);
  std::vector<long> v1;
  v1.push_back(20);

  std::set<std::vector<long> > se;
  assert("{?}" == protest::tostring::tostring(false, se));

  std::multiset<int> ms;
  assert("{?}" == protest::tostring::tostring(false, ms));

  std::map<std::string, int> ma;
  assert("{?}" == protest::tostring::tostring(false, ma));

  std::vector<bool> bove;
  bove.push_back(true);
  bove.push_back(false);
  assert("vector(true, false)" == protest::tostring::tostring(false, bove));

  std::bitset<10> bi;
  bi[0] = 1;
  bi[3] = 1;
  bi[5] = 1;
  bi[6] = 1;
  assert("0001101001" == protest::tostring::tostring(false, bi));

  std::complex<int> co(50, 60);
  assert("(50,60)" == protest::tostring::tostring(false, co));

  std::deque<const char*> de;
  assert("{?}" == protest::tostring::tostring(false, de));

  std::priority_queue<int> pd;
  assert("{?}" == protest::tostring::tostring(false, pd));

  std::multimap<std::string, int> mm;
  assert("{?}" == protest::tostring::tostring(false, mm));
  
  std::vector<Foo> foov(1);
  assert("vector(Foo)" == protest::tostring::tostring(false, foov));
}
