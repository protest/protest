#define PROTEST_USER_DEFINED_MAIN
#include "protest.hh"
#include <cassert>
#include "test-common.hh"

struct Reporter : public ::protest::Reporter {
  std::string message;
  void test_ignored(std::string const&, std::string const&,
		    std::string const& message) {
    this->message = message;
  }
};


bool setup = false;
bool test2 = true;

suite("suite 0") {
  setup = true;

  ignored("ignored test")
  test("test 0") {
    assert(false);
  }

  ignored("ignored test")
  test("test 1") {
    assert(false);
  }

  test("test 2") {
    test2 = true;
  }

}


int main() {
  {
    Reporter reporter;
    protest::context().reporter = &reporter;
    run_test("suite 0", "test 0");
    assert(reporter.message == "ignored test");
    assert(setup == false);
  }
  {
    Reporter reporter;
    protest::context().reporter = &reporter;
    run_test("suite 0", "test 1");
    assert(reporter.message == "ignored test");
    assert(setup == false);
  }
  {
    Reporter reporter;
    protest::context().reporter = &reporter;
    run_test("suite 0", "test 2");
    assert(reporter.message == "");
    assert(setup == true);
    assert(test2 == true);
  }
}
