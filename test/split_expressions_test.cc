#define PROTEST_USER_DEFINED_MAIN
#include "protest.hh"
#include <cassert>
#include <cstdio>


void compare(std::string const& lhs, std::string const& rhs, int line) {
  if (lhs != rhs) {
    printf("%s:%d: %s != %s\n", __FILE__, line, lhs.c_str(), rhs.c_str());
    assert(false);
  }
}

#define CHECK_SPLIT(expression, LHS, RHS)				\
  do {									                        \
    protest::ExprGrabber lhs, rhs;				    	\
    lhs.hexify = false;                         \
    rhs.hexify = false;                         \
    (void)(lhs < expression << rhs);				  	\
    compare(lhs.str(), LHS, __LINE__);					\
    compare(rhs.str(), RHS, __LINE__);					\
  } while (0)

int main() {
  CHECK_SPLIT(0 == 1, "0", "1");

  CHECK_SPLIT(0 - 1 == 0 + 1, "-1", "1");
  CHECK_SPLIT(0 - 1 != 0 + 1, "-1", "1");
  CHECK_SPLIT(0 - 1 <  0 + 1, "-1", "1");
  CHECK_SPLIT(0 - 1 <= 0 + 1, "-1", "1");
  CHECK_SPLIT(0 - 1 >= 0 + 1, "-1", "1");
  CHECK_SPLIT(0 - 1 >  0 + 1, "-1", "1");

  CHECK_SPLIT(0 == 10 + 1 * 20, "0", "30");
  CHECK_SPLIT(10 + 1 * 20 == 0, "30", "0");
  CHECK_SPLIT(10 << 1 == 0, "20", "0");
  CHECK_SPLIT(0 == 10 << 1, "0", "20");

  CHECK_SPLIT(3 & 1 == 0, "3", "0"); // == has higher priority than &
  CHECK_SPLIT(2 | 1 == 1, "2", "1"); // == has higher priority than |

}
