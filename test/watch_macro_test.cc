#define PROTEST_USER_DEFINED_MAIN
#include "protest.hh"
#include <cassert>
#include "test-common.hh"

size_t line;
struct Reporter : public protest::Reporter {
  std::string expr;
  std::string value;
  std::string _file, file;
  size_t _line, line;
  void progress(const char* file, size_t line) {
    this->_file = file;
    this->_line = line;
  }
  void watched_expression(std::string const& expr, std::string const& value) {
    this->expr = expr;
    this->value = value;
    this->file = _file;
    this->line = _line;
  }
};

suite("suite") {
  test("test 1") {
    int foo = 20;
    line = __LINE__; watch(foo);
  }
}
int main() {
  Reporter reporter;
  protest::context().reporter = &reporter;
  run_test("suite", "test 1");
  assert(reporter.expr == "foo");
  assert(reporter.value == "20");
  assert(reporter.file == __FILE__);
  assert(reporter.line == line);
}
