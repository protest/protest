#define PROTEST_USER_DEFINED_MAIN
#include "protest.hh"
#include <cassert>
#include "test-common.hh"
#include <stdio.h>
struct Reporter : public ::protest::Reporter {
  protest::Reporter::TestState state;
  void test_state(TestState state) {
    this->state = state;
  }
};

bool test0 = false;
bool test1a = false;
bool test1b = false;
bool test2 = false;
bool setup = false;
suite("suite 0") {
  setup = true;

  broken("broken test 0")
  test("test 0") {
    test0 = true;
  }

  broken("broken test 1")
  test("test 1") {
    test1a = true;
    check(false == true);
    test1b = true;
  }

  test("test 2") {
    test2 = true;
  }
}


int main() {
  {
    Reporter reporter;
    protest::context().reporter = &reporter;
    run_test("suite 0", "test 0");
    assert(setup == true);
    assert(test0 == true);
    assert(reporter.state == protest::Reporter::TEST_FAILED);
  }
  setup = false;
  {
    Reporter reporter;
    protest::context().reporter = &reporter;
    run_test("suite 0", "test 1");
    assert(setup == true);
    assert(test1a == true);
    assert(test1b == false);
    assert(reporter.state == protest::Reporter::TEST_PASSED);
  }
  setup = false;
  {
    Reporter reporter;
    protest::context().reporter = &reporter;
    run_test("suite 0", "test 2");
    assert(setup == true);
    assert(test2 == true);
    assert(reporter.state == protest::Reporter::TEST_PASSED);
  }
}
