#define PROTEST_USER_DEFINED_MAIN
#include "protest.hh"
#include "test-common.hh"
#include <cassert>


struct Reporter : public protest::Reporter {
  bool crashed;
  int line;
  Reporter() : crashed(false) { }

  void progress(const char*, size_t line) {
    this->line = line;
  }

  void test_state(protest::Reporter::TestState state) {
    if (state == protest::Reporter::TEST_CRASHED)
      crashed = true;
  }
};

int line = -1;
suite("suite") {
  test("test") {
    line = __LINE__ - 1;
    throw 1;
  }
}

int main() {
  Reporter reporter;
  protest::context().reporter = &reporter;
  run_test("suite", "test");
  assert(reporter.crashed);
  assert(reporter.line == line);
}
