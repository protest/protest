#define PROTEST_USER_DEFINED_MAIN
#include "protest.hh"
#include <cassert>
#include "test-common.hh"

struct Reporter : public protest::Reporter {
  std::string file;
  size_t line;
  void progress(const char* file, size_t line) {
    this->file = file;
    this->line = line;
  }
};


bool runned = false;
Reporter reporter;

bool check_line(size_t line) {
  assert(reporter.line == line);
  return true;
}

suite("suite") {
  assert(reporter.file == __FILE__);
  assert(reporter.line == __LINE__ - 2);
  test("test 0") {
    assert(reporter.file == __FILE__);
    assert(reporter.line == __LINE__ - 2);
    
    watch(1);
    assert(reporter.line == __LINE__ - 1);

    check(check_line(__LINE__));

    check(1 == 1);
    assert(reporter.line == __LINE__);
  }

  runned = true;
}

int main() {
  protest::context().reporter = &reporter;
  run_test("suite", "test 0");
  assert(runned == true);
}
