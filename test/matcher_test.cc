#define PROTEST_USER_DEFINED_MAIN
#include "protest.hh"
#include <cassert>
#include "test-common.hh"


struct Reporter : public protest::Reporter {
  std::string file;
  size_t line;
  std::string expr;
  std::string details;
  std::string message;

  Reporter() : line(0) { }

  void check_fail(const char* expr,
		  std::string const& details,
		  protest::OrdSet<std::string> const& /*seqvars*/,
		  std::string const& message) {
    this->file = file;
    this->line = line;
    this->expr = expr;
    this->details = details;
    this->message = message;
  }
};

#define is_zero(expr, message)				\
  PROTEST_MATCHER_BEGIN					\
  if (expr != 0)					\
    PROTEST_MATCHER_FAIL(message);			\
  else							\
    PROTEST_MATCHER_PASS();				\
  PROTEST_MATCHER_END

suite("suite 0") {
  test("test 0") {
    check(is_zero(0, "msg 0"));
    check(is_zero(1, "msg 0"));
  }
}
 
int main() {
  Reporter reporter;
  protest::context().reporter = &reporter;

  run_test("suite 0", "test 0");
  assert(reporter.details == "msg 0");
}

