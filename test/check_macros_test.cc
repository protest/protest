#define PROTEST_USER_DEFINED_MAIN
#include "protest.hh"
#include <cassert>
#include <sstream>
#include <iostream>
#include "test-common.hh"

size_t CORRECT_LINE = 0;
struct Reporter : public protest::Reporter {
  std::string _file, file;
  size_t _line, line;
  std::string expr;
  std::string details;
  std::string message;
  bool test_failure;

  Reporter() : line(0), test_failure(false) {}

  void check_fail(const char* expr,
		  std::string const& details,
		  protest::OrdSet<std::string> const& /*seqvars*/,
		  std::string const& message) {
    this->file = _file;
    this->line = _line;
    this->expr = expr;
    this->details = details;
    this->message = message;
  }

  void progress(const char* file, size_t line) {
    this->_file = file;
    this->_line = line;
  }

  void test_state(protest::Reporter::TestState state) {
    if (state == protest::Reporter::TEST_FAILED)
      test_failure = true;
  }

};
 
void throw_exception() {
  throw int(1);
}

bool test_stopped = true;
suite("suite 0") {
  test("test 0") {
     CORRECT_LINE = __LINE__; check(0 == 1);
  }
  test("test 1") {
    check(0 == 1) << "message" << 0;
  }

  test("test 2") {
    check(raises(throw_exception(), int));
  }

  test("test 3") {
    check(raises(0, int));
    test_stopped = false;
  }

  int one = 1;
  int zero = 0;
  test("test !=")    { check(one != one); }
  test("test <")     { check(one < one); }
  test("test <=")    { check(one <= zero); }
  test("test >")     { check(one > one); }
  test("test >=")    { check(zero >= one); }
  test("test no-op") { check(zero); }
}

#define compare(actual, expected)					\
  if (actual != expected) {						\
    std::cerr << __FILE__ << ":" <<__LINE__ << ": '" << actual		\
	      << "' != '" << expected << "'" << std::endl;		\
    assert(false);							\
  }

int main() {
  Reporter reporter;
  protest::context().reporter = &reporter;

  run_test("suite 0", "test 0");
  assert(reporter.file == __FILE__);
  assert(reporter.line == CORRECT_LINE);
  assert(reporter.expr == "0 == 1");
  assert(reporter.details == "0 == 1");
  assert(reporter.test_failure == true);
  assert(reporter.message == "");

  reporter = Reporter();
  run_test("suite 0", "test 1");
  assert(reporter.message == "message0");

  reporter = Reporter();
  run_test("suite 0", "test 2");
  assert(reporter.expr == "");
  assert(reporter.details == "");

  reporter = Reporter();
  run_test("suite 0", "test 3");
  assert(reporter.expr == "raises(0, int)");
  assert(reporter.details == "expected to throw int, but didn't");
  assert(test_stopped);

  
  reporter = Reporter();
  run_test("suite 0", "test !=");
  compare(reporter.details, "1 != 1");

  reporter = Reporter();
  run_test("suite 0", "test <");
  compare(reporter.details, "1 < 1");

  reporter = Reporter();
  run_test("suite 0", "test <=");
  compare(reporter.details, "1 <= 0");

  reporter = Reporter();
  run_test("suite 0", "test >");
  compare(reporter.details, "1 > 1");

  reporter = Reporter();
  run_test("suite 0", "test >=");
  compare(reporter.details, "0 >= 1");

  reporter = Reporter();
  run_test("suite 0", "test no-op");
  compare(reporter.details, "0");

}
