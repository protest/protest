#define PROTEST_MAIN
#include "protest-configured.hh"

SUITE("my suite") {
  BROKEN("broken!")
  IGNORED("don't run this")
  TEST("my test") {
    WATCH(1);
    CHECK(RAISES(1, int));
    FAIL();
  }

}
