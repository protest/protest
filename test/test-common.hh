namespace protest {
void visit_testcases(void* context, void (*func)(void*, std::string const&, std::string const&, TestCase const&));
}

void do_run_test(void* _testnames, std::string const& suitename, std::string const& testname, protest::TestCase const& tc)  {
  std::pair<std::string, std::string> const& names = *reinterpret_cast<std::pair<std::string, std::string>*>(_testnames);
  if (suitename == names.first && testname == names.second) {
    protest::context().run_test(suitename, testname, tc);
  }
}

void run_test(std::string const& suitename, std::string const& testname) {
  std::pair<std::string, std::string> names(suitename, testname);
  visit_testcases(&names, &do_run_test);
}


void do_run_all_tests(void*, std::string const& suitename, std::string const& testname, protest::TestCase const& tc)  {
  protest::context().run_test(suitename, testname, tc);
}

void run_all_tests() {
  visit_testcases(NULL, &do_run_all_tests);
}
