#define PROTEST_USER_DEFINED_MAIN
#include "protest.hh"
#include <cassert>
#include "test-common.hh"

struct Reporter : public protest::Reporter {
  bool crashed;
  int line;
  Reporter() : crashed(false) { }

  void progress(const char*, size_t line) {
    this->line = line;
  }

  void test_state(protest::Reporter::TestState state) {
    if (state == protest::Reporter::TEST_CRASHED)
      crashed = true;
  }

};


int* (*function)();

int line0, line1;
bool runned = false;
suite("suite") {
  test("test 0") {
    runned = true;
    check(1 == 1);
    line0 = __LINE__; int i = *function();
    (void)i;
  }

  test("test 1") {
    runned = true;
    line1 = __LINE__ - 2; assert(false && "it's ok, this is expected"); //(expect message)
  }
}

int main() {
  function = 0;
  Reporter reporter;
  protest::context().reporter = &reporter;
  run_test("suite", "test 0");
  assert(reporter.crashed);
  assert(runned == true);
  assert(reporter.line == line0);
  
  runned = false;
  reporter = Reporter();
  run_test("suite", "test 1");
  assert(reporter.crashed);
  assert(runned = true);
  assert(reporter.line == line1);
}
