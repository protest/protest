#define PROTEST_USER_DEFINED_MAIN
#include "protest.hh"

// This test tests that the implementation of the 'check' macro does
// not make the compiler report warnings on the wrong line. This can
// happen if, e.g., the macros are implemented using templates.

suite("suite") {
  test("test") {
    check(int(1) == unsigned(1)); //(expect message)
  }
}

suite("empty") { //(expect message)
}

int main() {
}
