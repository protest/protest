#define PROTEST_USER_DEFINED_MAIN
#include <protest.hh>
#include <string>
#include <cassert>
#include "test-common.hh"

void external_func() {
  fail();
}

bool setup_done;
bool teardown_done;
bool test0;
bool test1;
bool test1_passed;
bool test2;
bool test2_passed;
bool test_failed;

struct Reporter : public ::protest::Reporter {
  void test_state(protest::Reporter::TestState state) {
    if (state == protest::Reporter::TEST_FAILED)
      ::test_failed = true; 
  }
};

void reset() {
  setup_done = false;
  teardown_done = false;
  test0 = false;
  test1 = false;
  test1_passed = false;
  test2 = false;
  test2_passed = false;
  test_failed = false;
}

suite("suite") {
  setup_done = true;
    
  test("test 0") {
    test0 = true;
  }
  
  test("test 1") {
    test1 = true;
    fail();
    test1_passed = true;
  }

  test("test 2") {
    test2 = true;
    external_func();
    test2_passed = false;
  }

  teardown_done = true;
}


int main() {
  Reporter reporter;
  protest::context().reporter = &reporter;
  reset();
  run_test("suite", "test 0");
  assert(test0);
  assert(!test1);
  assert(setup_done);
  assert(teardown_done);

  reset();
  run_test("suite", "test 1");
  assert(!test0);
  assert(test1);
  assert(!test1_passed);
  assert(test_failed);
  assert(setup_done);
  assert(teardown_done);

  reset();
  run_test("suite", "test 2");
  assert(!test0);
  assert(!test1);
  assert(test2);
  assert(!test2_passed);
  assert(test_failed);
  assert(setup_done);
  assert(teardown_done);

}
