#define PROTEST_USER_DEFINED_MAIN
#include "protest.hh"
#include <csignal>
#include <cassert>
#include <csetjmp>
#include "test-common.hh"

struct Reporter : public protest::Reporter {
  bool crashed;
  void test_state(protest::Reporter::TestState state) {
    if (state == protest::Reporter::TEST_CRASHED)
      crashed = true;
  }
};


bool sigsegv_handled = false;
static sigjmp_buf sigsegv_buffer;
void sigsegv_handler(int) {
  sigsegv_handled = true;
  siglongjmp(sigsegv_buffer, 1);
}


int* (*function)();

suite("suite") {
  test("test 0") {
    int i = *function();
    (void)i;
  }
  test("test 1") {
    throw int(1);
  }
}

int main() {
  function = 0;
  Reporter reporter;
  protest::context().reporter = &reporter;
  protest::context().handle_signals = false;

  reporter.crashed = false;
  signal(SIGSEGV, &sigsegv_handler);
  if (sigsetjmp(sigsegv_buffer, 1) == 0) {
    run_test("suite", "test 0");
  }
  assert(sigsegv_handled == true);
  assert(reporter.crashed == false);
  {
    int caught = -1;
    try {
      run_test("suite", "test 1");
    } catch (int i) {
      caught = i;
    }
    assert(caught == -1);
  }


  protest::context().handle_signals = true;
  sigsegv_handled = false;
  if (sigsetjmp(sigsegv_buffer, 1) == 0) {
    run_test("suite", "test 0");
  }
  assert(sigsegv_handled == false);
  assert(reporter.crashed == true);

  protest::context().handle_exceptions = false;
  {
    int caught = -1;
    try {
      run_test("suite", "test 1");
    } catch (int i) {
      caught = i;
    }
    assert(caught == 1);
  }
}
