#define PROTEST_USER_DEFINED_MAIN
#include <protest.hh>
#include <string>
#include <cassert>
#include "test-common.hh"

struct Reporter : public ::protest::Reporter {
  int num_tests;
  Reporter() : num_tests(0) { }
  void test_state(protest::Reporter::TestState state) {
    if (state == protest::Reporter::SETUP_STARTED)
        num_tests++;
  }
};

suite("suite0") { // Must be at line 16!
  test("test 0") {
  }
}

/*
 Tests for bug where suites located at the same line (but in different
 files) collided and all but one was thrown away.
*/

int main() {
  Reporter reporter;
  protest::context().reporter = &reporter;
  run_test("suite0", "test 0");
  run_test("suite1", "test 1");

  assert(reporter.num_tests == 2);
}
