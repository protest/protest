#define PROTEST_USER_DEFINED_MAIN
#include "protest.hh"
#include <cassert>
#include "test-common.hh"


struct Reporter : public protest::Reporter {
  bool detected;
  size_t leak;
  void memory_leaked(size_t bytes) {
    detected = true;
    leak = bytes;
  }
};

suite("suite") {
  test("test 0") {
    new int;
    delete new int;
  }

  test("test 1") {
    new int[10];
  }

  test("test 2") {
    // TODO: Uncommenting this code gives memory leak on msvc, why?
    //    check(1 == 1) << 1;
    //    watch(1);
  }
}


int main() {
  Reporter reporter;
  protest::context().reporter = &reporter;

  reporter.leak = 0;
  run_test("suite", "test 0");
  assert(reporter.leak == sizeof(int));

  reporter.leak = 0;
  run_test("suite", "test 1");
  assert(reporter.leak == sizeof(int) * 10);

  reporter.leak = 0;
  run_test("suite", "test 2");
  assert(reporter.leak == 0);

  protest::context().detect_memleaks = false;
  reporter.detected = false;
  run_test("suite", "test 0");
  assert(reporter.detected == false);
}
