#define PROTEST_USER_DEFINED_MAIN
#include "protest.hh"
#include <set>
#include "test-common.hh"
#include <cassert>

struct TestInfo {
  std::string suitename, testname, expr;
  TestInfo(std::string const& suitename, std::string const& testname,
	   std::string const& expr) :
    suitename(suitename), testname(testname), expr(expr) { }

  bool operator==(TestInfo const& rhs) const {
    return suitename == rhs.suitename &&
      testname == rhs.testname &&
      expr == rhs.expr;
  }

  bool operator<(TestInfo const& rhs) const {
    if (suitename != rhs.suitename)
      return suitename < rhs.suitename;
    else if (testname != rhs.testname)
      return testname < rhs.testname;
    else if (expr != rhs.expr)
      return expr < rhs.expr;
    return false;
  }

};

struct Reporter : public protest::Reporter {
  std::string suitename, testname, expr;
  std::set<TestInfo> tests;

  void test_started(std::string const& suitename, std::string const& testname) {
    this->suitename = suitename;
    this->testname = testname;
  }

  void test_state(protest::Reporter::TestState state) {
    if (state == protest::Reporter::TEST_PASSED)
      tests.insert(TestInfo(suitename, testname, expr));
  }

  void watched_expression(std::string const&,
			  std::string const& value) {
    this->expr = value;
  }


};

int main() {
  Reporter reporter;
  protest::context().reporter = &reporter;
  run_all_tests();

  std::set<TestInfo>::iterator end = reporter.tests.end();

  assert(end != reporter.tests.find(TestInfo("suite-0", "test-00", "0/00")));
  assert(end != reporter.tests.find(TestInfo("suite-0", "test-01", "0/01")));

  assert(end != reporter.tests.find(TestInfo("suite-1", "test-10", "1/10")));
  assert(end != reporter.tests.find(TestInfo("suite-1", "test-11", "1/11")));

  assert(end != reporter.tests.find(TestInfo("suite-2", "test-20", "2/20")));

  assert(end != reporter.tests.find(TestInfo("suite-3", "test-30", "3/30")));
  assert(end != reporter.tests.find(TestInfo("suite-3", "test-31", "3/31")));

  assert(end != reporter.tests.find(TestInfo("suite-4", "test-40", "4/40")));
}
